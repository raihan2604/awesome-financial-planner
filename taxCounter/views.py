from django.shortcuts import render, redirect
from .models import TaxModels
from .forms import TaxForm

# Create your views here.
def index(request):
    if request.method == 'POST':
        form = TaxForm(request.POST)
        if form.is_valid():
            form.save()
            obj = TaxModels.objects.all().last()
            gaji = obj.income
            nikah = obj.married
            anak = int(obj.kids)
            pkp = hitungpkp(gaji, nikah, anak)
            pajak = hitungPajak(pkp)
            pajak = str(pajak)
            
            args = {'form' : form,'pajak':pajak}
            TaxModels.objects.all().delete()
            return render(request, 'home_tax.html', args)
    else:
        form = TaxForm()
    return render(request, 'home_tax.html', {'form':form, 'pajak':'0'})

def hitungpkp(income, married, kids):
    ptkp = 54000000
    ptkp_istriAnak = 4500000
    if(married == 'yes'):
        ptkp += ptkp_istriAnak
    jumlahAnak = int(kids)
    ptkp += jumlahAnak*ptkp_istriAnak
    return income-ptkp
    

def hitungPajak(pkp):
    if(pkp<50000000):
        return 5/100*pkp
    elif(pkp<250000000):
        pajak = 15/100*(pkp-50000000)
        return pajak+2500000
    elif(pkp<500000000):
        pajak=  25/100*(pkp-250000000)
        return pajak+32500000
    else:
        pajak = 30/100*(pkp-500000000)
        return pajak+95000000