from django.db import models

# Create your models here.
class TaxModels(models.Model):
    income = models.IntegerField()
    married = models.CharField(max_length = 4, choices = [('yes', 'Yes'), ('no','No')])
    kids = models.CharField(max_length = 2, choices = [('0', '0'), ('1','1'),('2','2')])