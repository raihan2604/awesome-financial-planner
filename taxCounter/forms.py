from django import forms
from django.forms import widgets
from .models import TaxModels

class TaxForm(forms.ModelForm):
    class Meta:
        model = TaxModels
        fields = {'income','married','kids'}