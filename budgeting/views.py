from django.shortcuts import render, redirect
from .models import Budgeting
from .forms import BudgetingForm
from django.utils import timezone

# Create your views here.
def index(request):
    budgets = Budgeting.objects.all()
    
    january = Budgeting.objects.filter(month=1)
    february = Budgeting.objects.filter(month=2)
    march = Budgeting.objects.filter(month=3)
    april = Budgeting.objects.filter(month=4)
    may = Budgeting.objects.filter(month=5)
    june = Budgeting.objects.filter(month=6)
    july = Budgeting.objects.filter(month=7)
    august = Budgeting.objects.filter(month=8)
    september = Budgeting.objects.filter(month=9)
    october = Budgeting.objects.filter(month=10)
    november = Budgeting.objects.filter(month=11)
    december = Budgeting.objects.filter(month=12)

    months = [1,2,3,4,5,6,7,8,9,10,11,12]

    total_budgets = 0
    for budget in budgets:
        total_budgets += budget.value

    total_budgets_str = '{:0,.0f}'.format(total_budgets)

    response = {
        'form' : BudgetingForm,
        'budgets' : budgets,
        'months' : months,
        'total_budgets' : total_budgets_str,
        'january' : january,
        'february' : february,
        'march' : march,
        'april' : april,
        'may' : may,
        'june' : june,
        'july' : july,
        'august' : august,
        'september' : september,
        'october' : october,
        'november' : november,
        'december' : december,
    }

    return render(request, "home_budgeting.html", response)

def save(request):
    form = BudgetingForm(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        form.save()
        return redirect('budgeting:index')
    
    else:
        return redirect('budgeting:index')   

def delete(request, delete_id):
   Budgeting.objects.filter(id=delete_id).delete()
   return redirect('budgeting:index')