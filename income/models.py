from django.db import models

# Create your models here.
class incomemodels(models.Model):
    income = models.IntegerField()
    description = models.CharField(max_length = 10000)

class expensemodels(models.Model):
    expense = models.IntegerField()
    description = models.CharField(max_length = 10000)