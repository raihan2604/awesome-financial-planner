from django.db import models
from django.utils import timezone
from datetime import datetime, date

MONTH_CHOICES = [
    (1, 'January'),
    (2, 'February'),
    (3, 'March'),
    (4, 'April'),
    (5, 'May'),
    (6, 'June'),
    (7, 'July'),
    (8, 'August'),
    (9, 'September'),
    (10, 'October'),
    (11, 'November'),
    (12, 'December'),
]

# Create your models here.
class Budgeting(models.Model):
    description = models.CharField(max_length=20)
    value = models.BigIntegerField()
    month = models.IntegerField(choices=MONTH_CHOICES)


    def __str__(self):
        return self.description
        