from django.contrib import admin
from .models import incomemodels, expensemodels

# Register your models here.
admin.site.register(incomemodels)
admin.site.register(expensemodels)