from django.test import TestCase, Client
from .models import liabilitymodels
from . import views
from django.urls import reverse, resolve
from .forms import *

class liability_test(TestCase):

    def test_form(self):
        form_data = {
            'Nama': 'tes',
            'duedate':'2000-10-20',
            'value':1000
        }
        form = LiabilityForm(data = form_data)
        self.assertFalse(form.is_valid())
    
    def test_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_url1(self):
        response = Client().get('/liability/input/')
        self.assertEqual(response.status_code, 200)

    def test_url2(self):
        response = Client().get('/liability/input/')
        self.assertEqual(response.status_code, 200)
    
    def createLiability(self, nama="tes", duedate='2000-10-20', value=1000):
        return liabilitymodels.objects.create(nama=nama, duedate = duedate, value = value)
    
    def test_input(self):
        p = self.createLiability()
        self.assertEqual(p.nama,"tes")
        self.assertEqual(p.duedate, '2000-10-20')
        self.assertEqual(p.value, 1000)

# Create your tests here.
