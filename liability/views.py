from django.shortcuts import render, redirect
from .models import liabilitymodels
from .forms import LiabilityForm

# Create your views here.
def index(request):
    obj = liabilitymodels.objects.all()
    utang = 0
    for x in obj:
        utang+=x.value
    utang_format = '{:0,.0f}'.format(utang)
    return render(request, "home_liability.html", {'form':obj,'utang':utang_format})

def paid(request, id):
    all = liabilitymodels.objects.get(id=id)
    all.delete()
    return redirect('/liability/')

def modify(request, id):
    all = liabilitymodels.objects.get(id=id)
# test


def input(request):
    if request.method == 'POST':
        form = LiabilityForm(request.POST)
        if form.is_valid():
            form.save()
            obj = liabilitymodels.objects.all()
            return redirect('/liability/')
    else:
        form = LiabilityForm()
        return render(request, "input.html", {'form':form})

def modify(request, id):
    if request.method == 'POST':
        form = LiabilityForm(request.POST)
        if form.is_valid():
            x = liabilitymodels.objects.get(id=id)
            print(x)
            x.nama = form.cleaned_data['nama']
            x.value = form.cleaned_data['value']
            x.duedate = form.cleaned_data['duedate']
            x.save()
            return redirect('/liability/')
    else:
        form = LiabilityForm()
    return render(request,'modify.html', {'form':form})
