from django.test import TestCase, Client
from .models import TaxModels
from . import views
from django.urls import reverse, resolve
from .forms import TaxForm

class TaxText(TestCase):

    def test_tax_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)


    def createPajak(self, gaji = 658500000, nikah = "yes", anak = "0"):
        return TaxModels.objects.create(income = gaji, married = nikah, kids = anak)

    def test_input(self):
        p = self.createPajak()
        self.assertEqual(p.income, 658500000)
        self.assertEqual(p.married, "yes")
        self.assertEqual(p.kids, "0")

    def test_pkp(self):
        p = self.createPajak()
        pkp = views.hitungpkp(p.income, p.married, p.kids)
        self.assertEqual(pkp, 600000000)
    
    def test_pajak(self):
        p = self.createPajak()
        pkp = views.hitungpkp(p.income, p.married, p.kids)
        pajak = views.hitungPajak(pkp)
        self.assertEqual(pajak, 125000000)

    def test_pajak_2(self):
        p = TaxModels.objects.create(income = 354000000, married = "no", kids = "0")
        pkp = views.hitungpkp(p.income, p.married, p.kids)
        pajak = views.hitungPajak(pkp)
        self.assertEqual(pajak, 45000000)
    
    def test_pajak_3(self):
        p = TaxModels.objects.create(income = 144000000, married = "no", kids = "0")
        pkp = views.hitungpkp(p.income, p.married, p.kids)
        pajak = views.hitungPajak(pkp)
        self.assertEqual(pajak, 8500000)


    def test_form(self):
        form_data = {
        "income" : 99000000,
        "married" : "yes",
        "kids" : "0",
        }
        form = TaxForm(data = form_data)
        self.assertTrue(form.is_valid())

        request = self.client.post('/taxCounter/', data = form_data)
        self.assertEqual(request.status_code, 200)

    def test_kosong(self):
        url = reverse('index')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
