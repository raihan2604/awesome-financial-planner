from django import forms
from django.forms import widgets
from .models import incomemodels, expensemodels

class IncomeForm(forms.ModelForm):
    class Meta:
        model = incomemodels
        fields = {'income','description'}

class ExpenseForm(forms.ModelForm):
    class Meta:
        model = expensemodels
        fields = {'expense','description'}
