from django.shortcuts import render, redirect
from .models import incomemodels, expensemodels
from .forms import IncomeForm, ExpenseForm


# Create your views here.
def index(request):
    obj = expensemodels.objects.all()
    obj2 = incomemodels.objects.all()
    balance = 0
    for a in obj:
        balance -= a.expense

    for b in obj2:
        balance += b.income
    
    balance_str = '{:0,.0f}'.format(balance)
    arg = {
        'balance' : balance_str,
        'income' : obj2,
        'expense' : obj
    }
    return render(request, 'home_income.html', arg)

def expense(request):
    if request.method == 'POST':
        form = ExpenseForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/income/')
    else:
        form = ExpenseForm()
    return render(request, "form_expense.html", {'form' : form})

def income(request):
    if request.method == 'POST':
        form = IncomeForm(request.POST)
        if form.is_valid():
            form.save()
            print("kesave bos")
            return redirect('/income/')
    else:
        form = IncomeForm()
    return render(request, "form_income.html", {'form' : form})

def income_delete(request,id):
    obj1 = incomemodels.objects.get(id=id)
    obj1.delete()
    return redirect('/income/')

def expense_delete(request,id):
    obj2 = expensemodels.objects.get(id=id)
    obj2.delete()
    return redirect('/income/')
